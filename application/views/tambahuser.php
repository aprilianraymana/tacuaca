<div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-5">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg-12">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Tambah User</h1>
              </div>
              <form class="user" action="<?php echo base_url();?>index.php/user/insert_user" method="post">
                <div class="form-group row">
                </div>
                <div class="form-group">
                  <input type="text" name="nama" class="form-control form-control-user" id="exampleInputnama" placeholder="Nama">
                </div>
                <div class="form-group">
                  <input type="email" name="email" class="form-control form-control-user" id="exampleInputemail" placeholder="Email">
                </div>
                <div class="form-group">
                  <input type="text" name="username" class="form-control form-control-user" id="exampleInputusername" placeholder="Username">
                </div>
                <div class="form-group">
                  <input type="password" name="password" class="form-control form-control-user" id="exampleInputpassword" placeholder="Password">
                </div>
                <div class="form-group row">
                </div>
                <button type="submit" class="btn btn-primary btn-user btn-block">Tambah</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>
    </div>

  <!-- </div> -->