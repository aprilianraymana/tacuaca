<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class cuacauser extends CI_Controller {

	public function index()
	{
		$data['bulan'] = $this->input->post('bulan');
		// print_r($data['bulan']);die;
		$this->load->model('ModelCuacaUser');
		if (!$data['bulan'] == null) {
			$data['cuaca'] = $this->ModelCuaca->getCuacaByBulan($data['bulan']);
			$data['cerah_berawan'] = $this->ModelCuaca->getCB($data['bulan']);
			$data['mendung'] = $this->ModelCuaca->getMendung($data['bulan']);
			$data['cerah'] = $this->ModelCuaca->getCerah($data['bulan']);
			$data['berawan'] = $this->ModelCuaca->getBerawan($data['bulan']);
			$data['berkabut'] = $this->ModelCuaca->getBerkabut($data['bulan']);
			$data['berangin_dan_mendung'] = $this->ModelCuaca->getBDM($data['bulan']);
			$data['berangin_dan_berawan'] = $this->ModelCuaca->getBDB($data['bulan']);
			$this->load->view('componentuser/headeruser');
			$this->load->view('componentuser/sidebaruser');
			$this->load->view('cuacabulananuser',$data);
			$this->load->view('componentuser/footeruser');	
		}else{
			$data['cuaca'] = $this->ModelCuacaUser->getCuaca($data['bulan']);
			$this->load->view('componentuser/headeruser');
			$this->load->view('componentuser/sidebaruser');
			$this->load->view('cuacabulananuser',$data);
			$this->load->view('componentuser/footeruser');
		}
	}
	
}