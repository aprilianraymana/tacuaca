  <!-- Begin Page Content -->
  <div class="container-fluid">

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
      <div class="card-header py-3">
          <form action="<?php echo base_url();?>index.php/tanaman/indexTanaman" method="post">
          <label for="cars">Pilih Musim:</label>

<select name="bulan">
  <option value="">Pilih Musim</option>
  <option value="kemarau">Kemarau</option>
  <option value="hujan">Hujan</option>
  <option value="semua_musim">Semua Musim</option>
</select>
<button type="submit">Cari</button>
    </form>
        <h6 class="m-0 font-weight-bold text-primary">Tanaman Musim</h6>
        <h4 class="title"><a href='<?= base_url();?>index.php/tanaman/tambahtanaman/' class="btn btn-success btn-fill" type="button" id="btn-input"><i class="fa fa-plus"></i></a></h4>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>Nama Tanaman</th>
                <th>Jenis Tanaman</th>
                <th>Musim</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
                <?php foreach($tanaman as $tanaman) { ?>
              <tr>
                <td><?php echo $tanaman->nama_tanaman;?></td>
                <td><?php echo $tanaman->jenis_tanaman;?></td>
                <td><?php echo $tanaman->musim;?></td>
                <td><button type="button" class="btn btn-info btn-circle" data-toggle="modal" data-target="#ModalUpdate<?php echo $tanaman->id;?>"><i class="fas fa-edit"></i></button> 
                <a href="<?php echo base_url('index.php/tanaman/deletetanaman/'.$tanaman->id);?>" class="btn btn-danger btn-circle"><i class="fas fa-trash"></i></a> 
                </td>
              </tr>
              <!-- Modal -->
              <div id="ModalUpdate<?php echo $tanaman->id;?>" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Edit Tanaman Musim</h4>
                        </div>
                        <div class="modal-body">
                        <form action="<?php base_url();?>index.php/tanaman/updateTanaman" method="post">
                         <input type="text" name="id" value="<?php echo $tanaman->id;?>" hidden>
                         <input type="text" name="nama_tanaman" value="<?php echo $tanaman->nama_tanaman;?>" readonly>
                         <input type="text" name="jenis_tanaman" value="<?php echo $tanaman->jenis_tanaman;?>">
                         <input type="text" name="musim" value="<?php echo $tanaman->musim;?>">
                        </div>
                        <div class="modal-footer">
                          <button type="submit" class="btn btn-default">Update</button>
                          </form>
                        </div>
                      </div>

                    </div>
                  </div>  
                <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>

  </div>
  <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->
<script>
$(document).ready( function () {
    $('#dataTable').DataTable();
} );
</script>