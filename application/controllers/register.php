<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class register extends CI_Controller {
	public function __construct() {
		parent :: __construct();
		$this ->load ->model('ModelUser');
		$this ->load ->helper('url_helper');
	}
	public function tambahuser()
	{
		$this->load->view('vregister');
	}
	public function insert_user()
	{
		$this->load->model('ModelUser');
		$user = array(
			'nama' => $this->input->post('nama'),
			'email' => $this->input->post('email'),
			'username' => $this->input->post('username'),
			'password' => $this->input->post('password'),
			"role"=>"user"
		);
		$this->ModelUser->insert_user($user);
		// print_r($test);die;

		redirect('login/index_login');
	}
}