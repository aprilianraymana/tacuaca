<?php
class ModelUser extends CI_model{
    public function __construct() {
        $this ->load -> database();
    }
    public function get_user() {
        return $this ->db ->get ('user') -> result();
    }
    public function user($nama) {
        return $this->db->get_where('user', array('nama' => $nama)) ->row();
    }
    public function insert_user($user)
    {
        return $this->db->insert('user', $user);
    }
    public function log($log)
    {
        return $this->db->insert('logs', $log);
    }
    public function update_user($data, $id)
    {
        $this->db->where('id_user',$id);
        return $this->db->update('user', $data);
    }
    public function deleteUser($id) {
        $this ->db ->where ('id_user', $id);
        $this ->db ->delete('user');
    }
}