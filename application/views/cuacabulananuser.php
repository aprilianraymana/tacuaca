  <!-- Begin Page Content -->
  <div class="container-fluid">

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
      <div class="card-header py-3">
          <form action="<?php echo base_url();?>index.php/cuacauser" method="post">
      <label for="cars">Pilih Bulan:</label>

<select name="bulan">
  <option value="">Pilih Bulan</option>
  <option value="Januari">Januari</option>
  <option value="Februari">Februari</option>
  <option value="Maret">Maret</option>
  <option value="April">April</option>
  <option value="Mei">Mei</option>
  <option value="Juni">Juni</option>
  <option value="Juli">Juli</option>
  <option value="Agustus">Agustus</option>
  <option value="September">September</option>
  <option value="Oktober">Oktober</option>
  <option value="November">November</option>
  <option value="Desember">Desember</option>
</select>
<button type="submit">Cari</button>
    </form>
    <?php if (!$bulan == null) { ?>
      Cerah Berawan <?php echo $cerah_berawan;?>
      Mendung <?php echo $mendung;?>
      Cerah <?php echo $cerah;?>
      Beawan <?php echo $berawan;?>
      Berkabut <?php echo $berkabut;?>
      Berangin Dan Mendung <?php echo $berangin_dan_mendung;?>
      Berangin Dan Berawan <?php echo $berangin_dan_berawan;?>
    <?php }else{ ?>

    <?php };?>
        <h6 class="m-0 font-weight-bold text-primary">Data Cuaca</h6>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>Tanggal</th>
                <th>Suhu</th>
                <th>Kelembaban</th>
                <th>Kecepatan Angin</th>
                <th>Klasifikasi</th>
                <th>Bulan</th>
              </tr>
            </thead>
            <tbody>
                <?php foreach($cuaca as $key) { ?>
              <tr>
                <td><?php echo $key->tanggal;?></td>
                <td><?php echo $key->suhu;?></td>
                <td><?php echo $key->kelembaban;?></td>
                <td><?php echo $key->kecepatan_angin;?></td>
                <td><?php echo $key->klasifikasi;?></td>
                <td><?php echo $key->bulan;?></td>
              </tr>
                    </div>
                  </div>
                <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>

  </div>
  <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->
<script>
$(document).ready( function () {
    $('#dataTable').DataTable();
} );
</script>