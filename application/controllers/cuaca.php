<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class cuaca extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->helper("url_helper");
        $this->load->library('session');
    }

	public function index()
	{
		$data['bulan'] = $this->input->post('bulan');
		// print_r($data['bulan']);die;
		$this->load->model('ModelCuaca');
		if (!$data['bulan'] == null) {
			$data['cuaca'] = $this->ModelCuaca->getCuacaByBulan($data['bulan']);
			$data['cerah_berawan'] = $this->ModelCuaca->getCB($data['bulan']);
			$data['mendung'] = $this->ModelCuaca->getMendung($data['bulan']);
			$data['cerah'] = $this->ModelCuaca->getCerah($data['bulan']);
			$data['berawan'] = $this->ModelCuaca->getBerawan($data['bulan']);
			$data['berkabut'] = $this->ModelCuaca->getBerkabut($data['bulan']);
			$data['berangin_dan_mendung'] = $this->ModelCuaca->getBDM($data['bulan']);
			$data['berangin_dan_berawan'] = $this->ModelCuaca->getBDB($data['bulan']);
			$this->load->view('test/header');
			$this->load->view('test/sidebar');
			$this->load->view('datacuaca2',$data);
			$this->load->view('test/footer');	
		}else{
			$data['cuaca'] = $this->ModelCuaca->getCuaca($data['bulan']);
			$this->load->view('test/header');
			$this->load->view('test/sidebar');
			$this->load->view('datacuaca2',$data);
			$this->load->view('test/footer');
		}
	}
	public function insert_cuaca()
	{
		$this->load->model('ModelCuaca');
		$cuaca = array(
			'tanggal' => $this->input->post('tanggal'),
			'suhu' => $this->input->post('suhu'),
			'kelembaban' => $this->input->post('kelembaban'),
			'kecepatan_angin' => $this->input->post('kecepatan_angin'),
			'bulan' => $this->input->post('bulan')
		);
		$this->ModelCuaca->insert_cuaca($cuaca);
		$log = array(
			'id_user' => $this->session->userdata('id_user'), 
			'kegiatan' => 'Tambah Cuaca',
			'tanggal' => date('Y-m-d')
		);
		// print_r($log);die;
		$this->ModelCuaca->log($log);
		// print_r($test);die;

		redirect('cuaca');
	}

	public function cek_perhitungan($id,$bulan){
		// print_r($id);die;
		$this->load->model('ModelCuaca');
		$cuaca = $this->ModelCuaca->getCuaca();
		$dataTest = $this->ModelCuaca->getDataTestCuaca($id,$bulan);
		$final2['test'] = $this->ModelCuaca->getDataTestCuaca($id,$bulan);
		// print_r($dataTest);die;
		$resp = [];
		foreach($cuaca as $key){
			// print_r($key);die;
			$param1 = ($key->suhu)-($dataTest->suhu);
			$pangkat1 = pow($param1,2);
			$param2 = ($key->kelembaban)-($dataTest->kelembaban);
			$pangkat2 = pow($param2,2);
			$param3 = ($key->kecepatan_angin)-($dataTest->kecepatan_angin);
			$pangkat3 = pow($param3,2);

			$hasil = $pangkat1 + $pangkat2 + $pangkat3;

			$final = [
				'id' => $key->id,
				'klasifikasi' => $key->klasifikasi,
				'hasil' => $hasil
			];

			// print_r($final);die;

			array_push($resp, $final);
		}

		usort($resp, function($a, $b){
			return $a['hasil'] <=> $b['hasil'];
		});

		$final2['hasil'] = [];

		for ($i=0; $i < 16; $i++) { 
			$suplay = $resp[$i];
			array_push($final2['hasil'], $suplay);
			
		}

		$this->load->view('test/header');
		$this->load->view('test/sidebar');
		$this->load->view('datacuaca3',$final2);
		$this->load->view('test/footer');
		
		

		
	}

	public function simpanHasil($id, $klasifikasi)
	{
		$this->load->model('ModelCuaca');

		$hasilKlasifikasi = str_replace('-',' ',$klasifikasi);
		
		$data = [
			'klasifikasi' => $hasilKlasifikasi
		];
// 
		// print_r($data); die;

		$update		= $this->ModelCuaca->updateHasil($id, $data);

		if ($update)
		{
			return redirect('cuaca');
			$log = array(
				'id_user' => $this->session->userdata('id_user'), 
				'kegiatan' => 'Hitung',
				'tanggal' => date('Y-m-d')
			);
			// print_r($log);die;
			$this->ModelCuaca->log($log);
			// print_r($test);die;
	
			redirect('cuaca');
		} else {
			echo 'Kesalahan';
		}
		
		// print_r($final2['hasil']);die;
	}

	public function tambahcuaca()
	{

		$this->load->view('test/header');
		$this->load->view('test/sidebar');
		$this->load->view('tambahcuaca');
		$this->load->view('test/footer');
	}

	public function updateCuaca()
	{
		$this->load->model('ModelCuaca');
		// print_r($id);die;
		$id = $this->input->post('id');

		$update = array(
			'suhu' => $this->input->post('suhu'),
			'kelembaban' => $this->input->post('kelembaban'),
			'kecepatan_angin' => $this->input->post('kecepatan_angin'),
			'klasifikasi' => $this->input->post('klasifikasi'),
			'bulan' => $this->input->post('bulan'),
		);
		$data['cuaca'] = $this->ModelCuaca->updateCuaca($update,$id);
		redirect('cuaca');
	}
	
	public function deleteCuaca($id)
	{		
		$this->load->model('ModelCuaca');
		$this->ModelCuaca->deleteCuaca($id);
		$log = array(
			'id_user' => $this->session->userdata('id_user'), 
			'kegiatan' => 'Hapus Cuaca',
			'tanggal' => date('Y-m-d')
		);
		// print_r($log);die;
		$this->ModelCuaca->log($log);
		// print_r($test);die;
		redirect('cuaca');
	}
	
}