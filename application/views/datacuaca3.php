  <!-- Begin Page Content -->
  <div class="container-fluid">

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
      <div class="card-header py-3">
      
        <h6 class="m-0 font-weight-bold text-primary">Data Cuaca</h6>
        <h4 class="title"><a href='<?= base_url();?>index.php/cuaca/tambahcuaca/' class="btn btn-success btn-fill" type="button" id="btn-input"><i class="fa fa-plus"></i></a></h4>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>Klasifikasi</th>
                <th>Nilai</th>
                <!-- <th>Aksi</th> -->
              </tr>
            </thead>
            <tbody>
            <?php 
            $clear = 0;
            $mcloudy = 0;
            $overcast = 0;
            $pcloudy = 0;
            $foggy = 0; ?>
                <?php foreach($hasil as $key) { ?>
                <?php if ($key['klasifikasi'] == 'Clear') {
                  $clear++;
                } else if ($key['klasifikasi'] == 'Mostly Cloudy') {
                  $mcloudy++;
                } else if ($key['klasifikasi'] == 'Partly Cloudy') {
                  $pcloudy++;
                } else if ($key['klasifikasi'] == 'Foggy') {
                  $foggy++;
                } else if ($key['klasifikasi'] == 'Overcast') {
                  $overcast++;
                } ?>
              <tr>
                <td><?php echo $key['klasifikasi'];?></td>
                <td><?php echo $key['hasil'];?></td>
                <!-- <td><button type="button" class="btn btn-info btn-circle" data-toggle="modal" data-target="#ModalUpdate<?php echo $key->id;?>"><i class="fas fa-edit"></i></button> 
                </td> -->
              </tr>
                <?php } ?>
            </tbody>
          </table>
          <center>
          <h5>Jumlah Clear: <?=$clear;?></h5>
          <h5>Jumlah Mostly Cloudy: <?=$mcloudy;?></h5>
          <h5>Jumlah Overcast: <?=$overcast;?></h5>
          <h5>Jumlah Partly Cloudy: <?=$pcloudy;?></h5>
          <h5>Jumlah Foggy: <?=$foggy;?></h5>

          <?php 
          $arrHasil = [
            [
              'klasifikasi'   => 'Clear',
              'nilai'         => $clear
            ],
            [
              'klasifikasi'   => 'Mostly Cloudy',
              'nilai'         => $mcloudy
            ],
            [
              'klasifikasi'   => 'Overcast',
              'nilai'         => $overcast
            ],
            [
              'klasifikasi'   => 'Partly Cloud',
              'nilai'         => $pcloudy
            ],
            [
              'klasifikasi'   => 'Foggy',
              'nilai'         => $foggy
            ]
          ];

          usort($arrHasil, function($a, $b){
            return $a['nilai'] <= $b['nilai'];
          });

          
          // print_r($arrHasil); die; ?>
          <br>
          <h3>Hasil Klasifikasi : <?= $arrHasil[0]['klasifikasi'];?></h3><br>
          <?php $hasilKlasifikasi = str_replace(' ','-',$arrHasil[0]['klasifikasi']); ?>
          <a class="btn btn-primary" href="<?=base_url();?>cuaca/simpanHasil/<?=$hasil[0]['id'];?>/<?=$hasilKlasifikasi;?>">Simpan Hasil</a>
          </center>

        </div>
      </div>
    </div>

  </div>
  <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->
<script>
$(document).ready( function () {
    $('#dataTable').DataTable();
} );
</script>