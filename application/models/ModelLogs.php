<?php
class ModelLogs extends CI_model{
    public function __construct() {
        $this ->load -> database();
    }
    public function get_logs() {
        return $this ->db ->get ('logs') -> result();
    }
}