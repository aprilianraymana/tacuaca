<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class dashboarduser extends CI_Controller {

	public function index()
	{
		$this->load->view('componentuser/headeruser');
		$this->load->view('componentuser/sidebaruser');
		$this->load->view('dashboarduser');
		$this->load->view('componentuser/footeruser');
	}
}
