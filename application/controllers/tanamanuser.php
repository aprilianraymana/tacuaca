<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class tanamanuser extends CI_Controller {
	public function __construct() {
		parent :: __construct();
		$this ->load ->model('ModelTanamanUser');
		$this ->load ->helper('url_helper');
	}
	public function indexTanaman()
	{
		$data['tanamanuser'] = $this->ModelTanamanUser->get_tanaman();
		$this->load->view('componentuser/headeruser');
		$this->load->view('componentuser/sidebaruser');
		$this->load->view('tanamanmusimuser', $data);
		$this->load->view('componentuser/footeruser');
	}
}