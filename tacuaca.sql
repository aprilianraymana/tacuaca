-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 09 Agu 2020 pada 21.48
-- Versi server: 10.4.13-MariaDB
-- Versi PHP: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tacuaca`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `datacuaca`
--

CREATE TABLE `datacuaca` (
  `id` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `suhu` varchar(50) NOT NULL,
  `kelembaban` varchar(50) NOT NULL,
  `kecepatan_angin` varchar(50) NOT NULL,
  `klasifikasi` varchar(50) NOT NULL,
  `bulan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `thujan`
--

CREATE TABLE `thujan` (
  `id` int(11) NOT NULL,
  `nma_tanaman` varchar(50) NOT NULL,
  `cra_menanam` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `thujan`
--

INSERT INTO `thujan` (`id`, `nma_tanaman`, `cra_menanam`) VALUES
(1, 'Sayur kangkung', 'Untuk media tanamnya juga tidak sulit, kangkung dapat di tanam di halaman rumah atau dapat juga di tanam dalam polybag.'),
(2, 'Sayur genjer', 'Untuk bercocok tanamsayuran ini, cukup cari bibit genjer yang tumbuh liar di alam. Anakan genjer dapat ditanam di lahan sawah dengan jarak 20-30 cm dan berikan pengairan yang cukup.'),
(3, 'Sayur bayam', 'Bagi kamu yang ingin bercocok tanam di pekarangan rumah, bedengan bisa dibuat dengan cara menumpuk bata berkeliling membentuk persegi (seperti membuat kolam), kemudian isi dengan campuran tanah dan pupuk kandang (1 : 1), dan pastikan kondisinya gembur. Atau bisa pula di pot atau polybag berdiameter '),
(4, 'Selada', 'Selada biasanya dikonsumsi sebagai pelengkap pada berbagai masakan seperti pada burger atau yang paling sering dijumpai sebagai lalapan pada pecel ayam atau pecel lele. Selada tumbuh baik di dataran tinggi, suhu 15-20 derajat celcius, pH tanah antara 5-6.5.'),
(5, 'Sawi putih', 'Saat awal tanam sawi membutuhkan air yang banyak. Namun untuk fase selanjutnya, sawi akan cepat membusuk jika diberi banyak air, sehingga akhir musim hujan adalah waktu yang tepat untuk menanam lada putih.\r\nSuhu yang tepat untuk tanaman sawi putih sekitar 19-20 derajat celcius dengan kelembapan udar'),
(6, 'Seledri', 'Seledri tidak membutuhkan banyak air dan rentan busuk jika terkena hujan. Untuk mengatasi hal ini, waktu yang tepat menanam seledri yaitu di akhir musim hujan dan ditanam di polybag.\r\nSaat masuk musim kemarau, akar tanaman seledri telah tumbuh kuat dan tidak gampang tercabut.'),
(7, 'Daun bawang', 'Daun bawang tumbuh optimal dengan curah hujan berkisar 1.500-2.000 mm/tahun sehingga cocok ditanam saat musim hujan. Suhu tumbuh tanaman bawang 18-25 derajat celsius dengan tingkat keasaman tanah pH 6.5-7.5.\r\nDaun bawang dapat ditanam di pekarangan rumah ataupun di dalam polybag dengan media tanam t');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tkemarau`
--

CREATE TABLE `tkemarau` (
  `id` int(11) NOT NULL,
  `nama_tanaman` varchar(50) NOT NULL,
  `cara_menanam` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tkemarau`
--

INSERT INTO `tkemarau` (`id`, `nama_tanaman`, `cara_menanam`) VALUES
(1, 'Tomat', 'Dapat tumbuh dengan baik di dataran tinggi maupun rendah\r\nWarna buah dipengaruhi suhu udara, yaitu pada temperatur tinggi di atas 32 derajat Celcius maka buah akan cenderung kuning\r\nPada temperature 24-28 derajat Celcius maka warna buah merata\r\nKelembaban yang tinggi memperburuk produksi dan kualita'),
(2, 'Terong', 'Dapat tumbuh dengan baik di dataran tinggi ataupun rendah\r\nSuhu udara yang baik yaitu antara 22 – 30 derajat Celcius\r\nMembutuhkan tanah subur, lempung berpasir, kaya bahan organik, dan berpH antara 6,8 – 7,3\r\nIntensitas matahari cukup\r\nCocok ditanam saat musim kemarau\r\nSaat pembentukan buah membutuh'),
(3, 'Mentimun', 'Cara menanam mentimun tidaklah sulit karena mentimun dapat beradaptasi dengan baik pada lingkungan tumbuhnya. Tanaman ini cocok ditanam pada musim kemarau. Tanaman mentimun tidak dapat hidup pada daerah yang terlalu lembab.'),
(4, 'Labu Siam', 'Cara menanam labu siam tergolong mudah karena labu siam dapat tumbuh subur tanpa memerlukan perawatan yang rumit. Waktu tanam yang baik yakni pada akhir musim hujan. Namun, tanaman ini juga bisa ditanam pada musim kemarau asalkan diberi air secukupnya.'),
(5, 'Jagung', 'Cara menanam jagung di musim kemarau tidaklah sulit. Tanaman ini dapat tumbuh dengan baik di daerah beriklim sedang. Tanaman jagung tidak membutuhkan curah hujan yang terlalu banyak.'),
(6, 'Singkong', 'Singkong dapat tumbuh dengan baik pada musim kemarau. Meskipun tanaman singkong memiliki syarat tumbuh yang ideal, tetapi tanaman ini juga dapat tumbuh di daerah yang tandus.\r\nTanaman ini tidak memerlukan perawatan yang sulit. Anda dapat mempelajari cara menanam singkong di lahan sempit jika tertari'),
(7, 'Ubi Jalar', 'Selain singkong, jenis umbi-umbian yang cocok ditanam pada musim kermarau adalah ubi jalar. Ubi jalar dapat bertahan hidup pada suhu udara tinggi. cara menanam ubi jalar juga tergolong mudah dan tanaman ini juga dapat dibudidayakan di lahan kering.');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id_user`, `nama`, `email`, `username`, `password`) VALUES
(1, 'Raymana Aprilian', 'aprilianraymana@gmail.com', 'admin', 'admin'),
(2, 'Nadin Amizah', 'cakecaine@gmail.com', 'user', 'user');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `datacuaca`
--
ALTER TABLE `datacuaca`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `thujan`
--
ALTER TABLE `thujan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tkemarau`
--
ALTER TABLE `tkemarau`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `datacuaca`
--
ALTER TABLE `datacuaca`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `thujan`
--
ALTER TABLE `thujan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `tkemarau`
--
ALTER TABLE `tkemarau`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
