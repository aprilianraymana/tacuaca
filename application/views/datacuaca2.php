<!-- Begin Page Content -->
  <div class="container-fluid">

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
      <div class="card-header py-3">
          <form action="<?php echo base_url();?>index.php/cuaca" method="post">
      <label for="cars">Pilih Bulan:</label>

<select name="bulan">
  <option value="">Pilih Bulan</option>
  <option value="Januari">Januari</option>
  <option value="Februari">Februari</option>
  <option value="Maret">Maret</option>
  <option value="April">April</option>
  <option value="Mei">Mei</option>
  <option value="Juni">Juni</option>
  <option value="Juli">Juli</option>
  <option value="Agustus">Agustus</option>
  <option value="September">September</option>
  <option value="Oktober">Oktober</option>
  <option value="November">November</option>
  <option value="Desember">Desember</option>
</select>
<button type="submit">Cari</button>
    </form>
    <?php if (!$bulan == null) { ?>
      Cerah Berawan <?php echo $cerah_berawan;?>
      Mendung <?php echo $mendung;?>
      Cerah <?php echo $cerah;?>
      Beawan <?php echo $berawan;?>
      Berkabut <?php echo $berkabut;?>
      Berangin Dan Mendung <?php echo $berangin_dan_mendung;?>
      Berangin Dan Berawan <?php echo $berangin_dan_berawan;?>
    <?php }else{ ?>

    <?php };?>
        <h6 class="m-0 font-weight-bold text-primary">Data Cuaca</h6>
        <h4 class="title"><a href='<?= base_url();?>index.php/cuaca/tambahcuaca/' class="btn btn-success btn-fill" type="button" id="btn-input"><i class="fa fa-plus"></i></a></h4>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>Tanggal</th>
                <th>Suhu</th>
                <th>Kelembaban</th>
                <th>Kecepatan Angin</th>
                <th>Klasifikasi</th>
                <th>Bulan</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
                <?php foreach($cuaca as $key) { ?>
              <tr>
                <td><?php echo $key->tanggal;?></td>
                <td><?php echo $key->suhu;?></td>
                <td><?php echo $key->kelembaban;?></td>
                <td><?php echo $key->kecepatan_angin;?></td>
                <td><?php echo $key->klasifikasi;?></td>
                <td><?php echo $key->bulan;?></td>
                <td><button type="button" class="btn btn-info btn-circle" data-toggle="modal" data-target="#ModalUpdate<?php echo $key->id;?>"><i class="fas fa-edit"></i></button> 
                <a href="<?php echo base_url('index.php/cuaca/deleteCuaca/'.$key->id);?>" class="btn btn-danger btn-circle"><i class="fas fa-trash"></i></a> 
                <?php if($key->klasifikasi == null){ ?>
                <a href="<?php echo base_url('index.php/cuaca/cek_perhitungan/'.$key->id.'/'.$key->bulan);?>" class="btn btn-info btn-circle"><i class="fas fa-calculator"></i></a> 
                <?php }else{ ?>

                <?php } ?>
                </td>
              </tr>
              <!-- Modal -->
              <div id="ModalUpdate<?php echo $key->id;?>" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                      <!-- Modal content-->
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Edit Data Cuaca</h4>
                        </div>
                        <div class="modal-body">
                        <form action="<?php base_url();?>index.php/cuaca/updateCuaca" method="post">
                         <input type="text" name="id" value="<?php echo $key->id;?>" hidden>
                         <input type="text" name="tanggal" value="<?php echo $key->tanggal;?>" readonly>
                         <input type="text" name="suhu" value="<?php echo $key->suhu;?>">
                         <input type="text" name="kelembaban" value="<?php echo $key->kelembaban;?>">
                         <input type="text" name="kecepatan_angin" value="<?php echo $key->kecepatan_angin;?>">
                         <input type="text" name="klasifikasi" value="<?php echo $key->klasifikasi;?>">
                         <input type="text" name="bulan" value="<?php echo $key->bulan;?>">
                        </div>
                        <div class="modal-footer">
                          <button type="submit" class="btn btn-default">Update</button>
                          </form>
                        </div>
                      </div>

                    </div>
                  </div>
                <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>

  </div>
  <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->
<script>
$(document).ready( function () {
    $('#dataTable').DataTable();
} );
</script>