<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class login extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->helper("url_helper");
        $this->load->model('ModelLogin');
        $this->load->model('ModelCuaca');
        $this->load->library('session');
    }
    public function index_login()
    {
        $this->load->view("vlogin");
    }
    public function proses_login()
    {
        
        $data = array(
            'username' => $this->input->post('username'),
            'password' => $this->input->post('pass')
        );
        // print_r ($data); die;
        $hasil = $this->ModelLogin->cek_login($data);
        // print_r($hasil);die;
        if ($hasil->num_rows() == 1) {
            foreach ($hasil->result() as $sess) {
                $sess_data['id_user'] = $sess->id_user;
                $sess_data['username'] = $sess->username;
                $sess_data['password'] = $sess->password;
                $sess_data['role'] = $sess->role;
                $sess_data['status_login'] = 'login';
                $sess_data['pw'] = $this->input->post('password');
                $this->session->set_userdata($sess_data);
            }
            $log = array(
                'id_user' => $this->session->userdata('id_user'), 
                'kegiatan' => 'Login',
                'tanggal' => date('Y-m-d'),
            );
            $this->ModelCuaca->log($log);

            if ($sess_data['id_user']) {
               if ($sess_data['role'] == 'admin') {
                   redirect('index.php/welcome/index/');
               }else{
                redirect('index.php/dashboarduser/index/');
               }
            }
        }
        else
    echo"<script>alert('username atau password anda salah!'); window.location = '../login'</script>";
    }
    public function index_logout()
    {
        session_destroy();
        redirect($route);
    }
}