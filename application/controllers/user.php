<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class user extends CI_Controller {
	public function __construct() {
		parent :: __construct();
		$this ->load ->model('ModelUser');
		$this ->load ->helper('url_helper');
		$this->load->library('session');
	}
	public function indexUser()
	{
		$data['user'] = $this->ModelUser->get_user();
		$this->load->view('test/header');
		$this->load->view('test/sidebar');
		$this->load->view('vuser', $data);
		$this->load->view('test/footer');
	}
	public function tambahuser()
	{
		$this->load->view('test/header');
		$this->load->view('test/sidebar');
		$this->load->view('tambahuser');
		$this->load->view('test/footer');
	}
	public function insert_user()
	{
		$this->load->model('ModelUser');
		$user = array(
			'nama' => $this->input->post('nama'),
			'email' => $this->input->post('email'),
			'username' => $this->input->post('username'),
			'password' => $this->input->post('password')
		);
		$this->ModelUser->insert_user($user);
		$log = array(
			'id_user' => $this->session->userdata('id_user'), 
			'kegiatan' => 'Tambah User',
			'tanggal' => date('Y-m-d')
		);
		// print_r($log);die;
		$this->ModelUser->log($log);
		// print_r($test);die;

		redirect('user/indexUser');
	}
	public function updateUser()
	{
		$this->load->model('ModelUser');
		// print_r($id);die;
		$id = $this->input->post('id_user');

		$update = array(
			'nama' => $this->input->post('nama'),
			'email' => $this->input->post('email'),
			'username' => $this->input->post('username'),
			'password' => $this->input->post('password'),
		);
		// print_r($update);die;
		$data['user'] = $this->ModelUser->update_User($update,$id);
		$log = array(
			'id_user' => $this->session->userdata('id_user'), 
			'kegiatan' => 'Update User',
			'tanggal' => date('Y-m-d')
		);
		// print_r($log);die;
		$this->ModelUser->log($log);
		// print_r($test);die;
		redirect('user/indexUser');
	}
	public function deleteUser($id)
	{		
		$this->load->model('ModelUser');
		$this->ModelUser->deleteUser($id);
		$log = array(
			'id_user' => $this->session->userdata('id_user'), 
			'kegiatan' => 'Hapus User',
			'tanggal' => date('Y-m-d')
		);
		// print_r($log);die;
		$this->ModelUser->log($log);
		// print_r($test);die;
		redirect('user/indexUser');
	}
	public function userEdit($nama)
	{
		$data['edit'] = $this->Modaluser->user($nama);
		$this->load->view('test/header');
		$this->load->view('test/sidebar');
		$this->load->view('edit', $data);
		$this->load->view('test/footer');
	}
	public function edituser()
	{
		$id = $this->input->post('no_Modaluser');
		$this->Modaluser->update_Modaluser($id);
		redirect('index.php/user/vuser');
	}
}