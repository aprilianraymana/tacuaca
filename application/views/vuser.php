  <!-- Begin Page Content -->
  <div class="container-fluid">

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
      <div class="card-header py-3">
          <form action="<?php echo base_url();?>index.php/user" method="post">
    </form>
        <h6 class="m-0 font-weight-bold text-primary">Data User</h6>
        <h4 class="title"><a href='<?= base_url();?>index.php/user/tambahuser/' class="btn btn-success btn-fill" type="button" id="btn-input"><i class="fa fa-plus"></i></a></h4>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>Nama</th>
                <th>Email</th>
                <th>Username</th>
                <th>Password</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
                <?php foreach($user as $user) { ?>
              <tr>
                <td><?php echo $user->nama;?></td>
                <td><?php echo $user->email;?></td>
                <td><?php echo $user->username;?></td>
                <td><?php echo $user->password;?></td>
                <td><button type="button" class="btn btn-info btn-circle" data-toggle="modal" data-target="#ModalUpdate<?php echo $user->id_user;?>"><i class="fas fa-edit"></i></button> 
                <a href="<?php echo base_url('index.php/user/deleteUser/'.$user->id_user);?>" class="btn btn-danger btn-circle"><i class="fas fa-trash"></i></a> 
                </td>
              </tr>
              <!-- Modal -->
              <div id="ModalUpdate<?php echo $user->id_user;?>" class="modal fade" role="dialog">
                    <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Edit Data User</h4>
                        </div>
                        <div class="modal-body">
                        <form action="<?php echo base_url('user/updateUser');?>" method="post">
                         <input type="text" name="id_user" value="<?php echo $user->id_user;?>" hidden>
                         <input type="text" name="nama" value="<?php echo $user->nama;?>" readonly>
                         <input type="text" name="email" value="<?php echo $user->email;?>">
                         <input type="text" name="username" value="<?php echo $user->username;?>">
                         <input type="text" name="password" value="<?php echo $user->password;?>">
                        </div>
                        <div class="modal-footer">
                          <button type="submit" class="btn btn-default">Update</button>
                          </form>
                        </div>
                      </div>

                    </div>
                  </div>  
                <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>

  </div>
  <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->
<script>
$(document).ready( function () {
    $('#dataTable').DataTable();
} );
</script>