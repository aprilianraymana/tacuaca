<?php
class ModelCuaca extends CI_model{
    public function __construct() {
        $this ->load -> database();
    }
    public function getCuaca() {
        return $this ->db ->get ('datacuaca') -> result();
    }

    public function getCuacaById($id) {
        return $this ->db ->get_where('datacuaca', array('id' =>$id)) -> result();
    }

    public function getDataTestCuaca($id,$bulan) {
        $this->db->select('*');
        $this->db->from('datacuaca');
        $this->db->where('id', $id);
        $this->db->where('bulan', $bulan);
        return $this->db->get()->row();
    }

    public function getCuacaByBulan($bulan) {
        return $this ->db ->get_where('datacuaca', array('bulan' => $bulan)) -> result();
    }

    public function getCB($bulan) {
        return $this ->db ->get_where('datacuaca', array('bulan' => $bulan, 'klasifikasi' => 'Cerah Berawan'))->num_rows();
    }

    public function getMendung($bulan) {
        return $this ->db ->get_where('datacuaca', array('bulan' => $bulan, 'klasifikasi' => 'Mendung'))->num_rows();
    }

    public function getCerah($bulan) {
        return $this ->db ->get_where('datacuaca', array('bulan' => $bulan, 'klasifikasi' => 'Cerah'))->num_rows();
    }

    public function getBerawan($bulan) {
        return $this ->db ->get_where('datacuaca', array('bulan' => $bulan, 'klasifikasi' => 'Berawan'))->num_rows();
    }

    public function getBerkabut($bulan) {
        return $this ->db ->get_where('datacuaca', array('bulan' => $bulan, 'klasifikasi' => 'Berkabut'))->num_rows();
    }
    public function getBDM($bulan) {
        return $this ->db ->get_where('datacuaca', array('bulan' => $bulan, 'klasifikasi' => 'Berangin Dan Mendung'))->num_rows();
    }
    public function getBDB($bulan) {
        return $this ->db ->get_where('datacuaca', array('bulan' => $bulan, 'klasifikasi' => 'Berangin Dan Berawan'))->num_rows();
    }
    public function insert_cuaca($cuaca)
    {
        return $this->db->insert('datacuaca', $cuaca);
    }

    public function updateCuaca($data, $id)
    {
        $this->db->where('id',$id);
        return $this->db->update('datacuaca', $data);
    }

    public function log($log)
    {
        return $this->db->insert('logs', $log);
    }

    public function deleteCuaca($id) {
        $this ->db ->where ('id', $id);
        $this ->db ->delete('datacuaca');
    }
    public function updateHasil($id, $data)
    {
        $this->db->where('id', $id);
        return $this->db->update('datacuaca', $data);
    }

}