<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class tanaman extends CI_Controller {
	public function __construct() {
		parent :: __construct();
		$this ->load ->model('ModelTanaman');
		$this ->load ->helper('url_helper');
		$this->load->library('session');
	}
	public function indexTanaman()
	{
		$data['tanaman'] = $this->ModelTanaman->get_tanaman();
		$this->load->view('test/header');
		$this->load->view('test/sidebar');
		$this->load->view('tanamanmusim', $data);
		$this->load->view('test/footer');
	}
	public function tambahtanaman()
	{
		$this->load->view('test/header');
		$this->load->view('test/sidebar');
		$this->load->view('tambahtanaman');
		$this->load->view('test/footer');
	}
	public function insert_tanaman()
	{
		$this->load->model('ModelTanaman');
		$tanaman = array(
			'nama_tanaman' => $this->input->post('nama_tanaman'),
			'cara_menanam' => $this->input->post('cara_menanam'),
			'musim' => $this->input->post('musim')
		);
		$this->ModelTanaman->insert_tanaman($tanaman);
		$log = array(
			'id_user' => $this->session->userdata('id_user'), 
			'kegiatan' => 'Tambah Tanaman',
			'tanggal' => date('Y-m-d')
		);
		// print_r($log);die;
		$this->ModelUser->log($log);
		// print_r($test);die;

		redirect('tanaman/indexTanaman');
	}
	public function updateTanaman()
	{
		$this->load->model('ModelTanaman');
		// print_r($id);die;
		$id = $this->input->post('id');

		$update = array(
			'nama_tanaman' => $this->input->post('nama_tanaman'),
			'cara_menanam' => $this->input->post('cara_menanam'),
			'musim' => $this->input->post('musim')
		);
		// print_r($update);die;
		$data['tanaman'] = $this->ModelTanaman->update_tanaman($update,$id);
		$log = array(
			'id_user' => $this->session->userdata('id_user'), 
			'kegiatan' => 'Update Tanaman',
			'tanggal' => date('Y-m-d')
		);
		// print_r($log);die;
		$this->ModelUser->log($log);
		// print_r($test);die;

		redirect('tanaman/indexTanaman');
	}
	public function deleteTanaman($id)
	{		
		$this->load->model('ModelTanaman');
		$this->ModelTanaman->deleteTanaman($id);
		$log = array(
			'id_user' => $this->session->userdata('id_user'), 
			'kegiatan' => 'Hapus Tanaman',
			'tanggal' => date('Y-m-d')
		);
		// print_r($log);die;
		$this->ModelUser->log($log);
		// print_r($test);die;

		redirect('tanaman/indexTanaman');
	}
	public function tanamanEdit($nama)
	{
		$data['edit'] = $this->Modaltanaman->tanaman($nama);
		$this->load->view('test/header');
		$this->load->view('test/sidebar');
		$this->load->view('edit', $data);
		$this->load->view('test/footer');
	}
	public function edittanaman()
	{
		$id = $this->input->post('no_Modaltanaman');
		$this->Modaltanaman->update_Modaltanaman($id);
		$log = array(
			'id_user' => $this->session->userdata('id_user'), 
			'kegiatan' => 'Edit Tanaman',
			'tanggal' => date('Y-m-d')
		);
		// print_r($log);die;
		$this->ModelUser->log($log);
		// print_r($test);die;

		redirect('tanaman/indexTanaman');
	}
}