<?php
class ModelTanamanUser extends CI_model{
    public function __construct() {
        $this ->load -> database();
    }
    public function get_tanaman() {
        return $this ->db ->get ('tanaman') -> result();
    }
    public function tanaman($nama_tanaman) {
        return $this->db->get_where('tanaman', array('nama_tanaman' => $nama_tanaman)) ->row();
    }
}