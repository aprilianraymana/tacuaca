  <!-- Begin Page Content -->
  <div class="container-fluid">

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
      <div class="card-header py-3">
          <form action="<?php echo base_url();?>index.php/tanaman/indexTanaman" method="post">
          <label for="cars">Pilih Musim:</label>

<select name="bulan">
  <option value="">Pilih Musim</option>
  <option value="Kemarau">Kemarau</option>
  <option value="Dingin">Dingin</option>
</select>
<button type="submit">Cari</button>
    </form>
        <h6 class="m-0 font-weight-bold text-primary">Tanaman Musim Hujan</h6>
      </div>
      <div class="card-body">
        <div class="table-responsive">
          <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>Nama Tanaman</th>
                <th>Jenis Tanaman</th>
                <th>Musim</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
                <?php foreach($tanaman as $tanaman) { ?>
              <tr>
                <td><?php echo $tanaman->nama_tanaman;?></td>
                <td><?php echo $tanaman->jenis_tanaman;?></td>
                <td><?php echo $tanaman->musim;?></td>
              </tr>
                    </div>
                  </div>  
                <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>

  </div>
  <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->
<script>
$(document).ready( function () {
    $('#dataTable').DataTable();
} );
</script>