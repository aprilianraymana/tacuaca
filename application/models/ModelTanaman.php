<?php
class ModelTanaman extends CI_model{
    public function __construct() {
        $this ->load -> database();
    }
    public function get_tanaman() {
        return $this ->db ->get ('tanaman') -> result();
    }
    public function tanaman($nama) {
        return $this->db->get_where('tanaman', array('nama' => $nama)) ->row();
    }
    public function insert_tanaman($tanaman)
    {
        return $this->db->insert('tanaman', $tanaman);
    }
    public function log($log)
    {
        return $this->db->insert('logs', $log);
    }
    public function update_tanaman($data, $id)
    {
        $this->db->where('id',$id);
        return $this->db->update('tanaman', $data);
    }
    public function deletetanaman($id) {
        $this ->db ->where ('id', $id);
        $this ->db ->delete('tanaman');
    }
}