<div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-5">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg-12">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Tambah Data Cuaca</h1>
              </div>
              <form class="user" action="<?php echo base_url();?>index.php/cuaca/insert_cuaca" method="post">
                <div class="form-group row">
                </div>
                <div class="form-group">
                  <!-- <label for="datepicker">Tanggal</label> -->
                  <input type="date" name="tanggal" class="form-control form-control-user" id="datepicker" placeholder="Tanggal">
                </div>
                <div class="form-group">
                  <input type="text" name="suhu" class="form-control form-control-user" id="exampleInputsuhu" placeholder="Suhu">
                </div>
                <div class="form-group">
                  <input type="text" name="kelembaban" class="form-control form-control-user" id="exampleInputkelembaban" placeholder="Kelembaban">
                </div>
                <div class="form-group">
                  <input type="text" name="kecepatan_angin" class="form-control form-control-user" id="exampleInputkecepatanangin" placeholder="Kecepatan Angin">
                </div>
                <div class="form-group">
                  <input type="text" name="bulan" class="form-control form-control-user" id="exampleInputbulan" placeholder="Bulan">
                </div>
                <div class="form-group row">
                </div>
                <button type="submit" class="btn btn-primary btn-user btn-block">Tambah</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>

  </div>