<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	public function __construct() {
		parent :: __construct();
		$this ->load ->model('ModelLogs');
		$this ->load ->helper('url_helper');
	}
	public function index()
	{
		$data['logs'] = $this->ModelLogs->get_logs();
		$this->load->view('test/header');
		$this->load->view('test/sidebar');
		$this->load->view('dashboard', $data);
		$this->load->view('test/footer');
	}
}
